# Literatur

* [Modern C, Jens Gustedt](http://icube-icps.unistra.fr/img_auth.php/d/db/ModernC.pdf) (Primär)
* Grundkurs C, Jürgen Wolf
* Head First C, David Griffiths & Dawn Griffiths

# Online-Referenzen

* [YouTube-Serie zu Parellelismus in C++ (auf C übertragbar)](https://www.youtube.com/playlist?list=PLzLzYGEbdY5lrUYSssHfk5ahwZERojgid)
  - Speziell für SIMD und OpenMP 
* [Ausführlicher Guide zu OpenMP mit C++ Fokus (auf C übertragbar)](https://bisqwit.iki.fi/story/howto/openmp/)
* [GCC-Optimierungen](https://wiki.gentoo.org/wiki/GCC_optimization)
* [Should you learn C to “learn how the computer works”?](https://words.steveklabnik.com/should-you-learn-c-to-learn-how-the-computer-works)
  - Diskutiert die Philosophie hinter C, die allgemein spezifizierte Architektur und geht auf die Geschichte von C ein
* [Compiler Explorer](https://godbolt.org/)
  - gut zum Illustrieren verschiedener Optimierungsstufen
