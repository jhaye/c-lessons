# C-Programmierkurs Überblick

1. Geschichte und erste Schritte
   - Geschichte von C
   - Einrichtung zum Kompilieren von C-Programmen
2. Schleifen und Matritzen
   - kleiner Unix-Exkurs
   - Programm zur einfachen Matrixmultiplikation
   - for-Schleifen
   - Arrays als Datenstruktur für Matritzen
   - Integer Datentypen in C
3. Funktionen und Zeichenketten
   - Programm zur ROT13-Verschlüsselung
   - Strings und char
   - Funktionen
   - While-Schleifen
   - if-Abfragen
   - Arbeiten mit Nutzerinput (scanf)
4. Arbeiten mit Dateien
   - Erweiterung des ROT13-Programms
   - Lesen/Schreiben von Dateien
   - Stdin/Stdout
   - read() aus unistd.h
   - Kommandozeilenargumente
5. Arbeiten mit Dateien II
6. Datenstrukturen, Speicher und Pointer
   - Pointer
   - Structs
   - malloc, (calloc, realloc), memcopy, free
7. Datenstrukturen, Speicher und Pointer II
   - #include von lokalen Dateien (selbst Biblotheken schreiben
   - NULL pointer
   - VLAs

### Ideenliste

* Compileroptionen
   - Optimierung
   - Warnings
   - Compiler-spezifische Makros
* Prallell processing
   - C11 Threading
   - OpenMP
* Benchmarking Game
