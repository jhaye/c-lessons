# C-Programmierkurs Folien

Die Folien für den C-Programmierkurs. Manche Stunden haben keine Folien, da an diesen Tagen nur praktisch gearbeitet wurde.

## Prerequisites

* `xelatex` zum Bauen
* eine LaTeX-Distribution, am besten TeX-Live
  - für spezifische Pakete einfach den Quelltext ansehen, besonders `macros.tex`
* für korrektes Font-Rendering, [Mozilla Fira Sans](https://github.com/bBoxType/FiraSans)
* [Pygments](http://pygments.org/) für Syntax-Highlighting

## Building

Die Folien werden mit dem `build.sh` building script gebaut.

### Nutzung

`build.sh` ohne weitere Parameter baut alle TeX-Dateien im Verzeichnis.

`build.sh clean` entfernt alle Nebendateien des Bauprozesses.

Wird als Parameter ein Präfix einer im Verzeichnis befindenden Data verwendet, werden alle Dateien mit diesem Präfix gebaut.
So baut `build.sh 01` die TeX-Datei `01_history_plus_fist_steps.tex`.
