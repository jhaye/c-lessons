#!/bin/bash

build_item() {
    # strip file extension
    file=$(echo "$1" | cut -f 1 -d '.')

    xelatex --shell-escape "$file"
    biber "$file"
    xelatex --shell-escape "$file"
}

if [ $# -eq 0 ]
then
    for tex in $PWD/*.tex
    do
	build_item "$tex"
    done
else
    if [ "$1" = "clean"  ]
    then
	rm - *.blg *.aux *.bbl *.bcf *.log *.nav *.out *.xml *.snm *.toc *.vrb
	rm -r _minted*
    else
	for tex in $PWD/$1*.tex
	do
	    build_item "$tex"
	done
    fi
fi
